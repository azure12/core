// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

'use strict';


require('dotenv').config({ path: '.tmp' })
var Mqtt = require('azure-iot-device-mqtt').Mqtt;
var DeviceClient = require('azure-iot-device').Client
var Message = require('azure-iot-device').Message;

const connectionString = process.env.IOT_DEVICE_CONNECTION_STRING;
var client = DeviceClient.fromConnectionString(connectionString, Mqtt);

function sendEvent() {
  const temperature = 20 + (Math.random() * 15);
  const message = new Message(JSON.stringify({
    temperature,
    humidity: 60 + (Math.random() * 20)
  }));

  message.properties.add('temperatureAlert', (temperature > 30) ? 'true' : 'false');
  console.log('Sending message: ' + message.getData());

  client.sendEvent(message, function (err) {
    if (err) {
      console.error('send error: ' + err.toString());
    } else {
      console.log('message sent');
    }
  });
}

// Create a message and send it to the IoT hub every second
setInterval(sendEvent, 1000);
