echo "Prepare ..."
source .env
rm .tmp
touch .tmp

echo '# Add extension ...'
echo '###################'
az extension add --name azure-iot

echo '# Create Resource Group  ...'
echo '############################'
az group create --name $RESOURCE_GROUP --location $LOCATION

echo '# Create IoT Hub  ...'
echo '#####################'
az iot hub create -n $IOT_HUB_NAME -l $LOCATION -g $RESOURCE_GROUP 

echo '# Create device identity ...'
echo '############################'
az iot hub device-identity create --hub-name $IOT_HUB_NAME --device-id $IOT_DEVICE_ID

echo '# Temporary values ...'
echo '###########################'
echo "IOT_DEVICE_IDENTITY=$(az iot hub device-identity connection-string show --hub-name $IOT_HUB_NAME --device-id $IOT_DEVICE_ID --output tsv)" >> .tmp
echo "EVENT_HUB_ENDPOINT=$(az iot hub show --query properties.eventHubEndpoints.events.endpoint --name $IOT_HUB_NAME)" >> .tmp
echo "EVENT_HUB_PATH=$(az iot hub show --query properties.eventHubEndpoints.events.path --name $IOT_HUB_NAME)" >> .tmp
echo "IOT_HUB_PRIMARY_KEY=$(az iot hub policy show --name service --query primaryKey --hub-name $IOT_HUB_NAME)" >> .tmp