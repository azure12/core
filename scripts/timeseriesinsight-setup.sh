
source .env 

echo "Create Storage account ..."
echo "##########################"
az storage account create -g $RESOURCE_GROUP -n $STORAGE_ACCOUNT_NAME

echo "Configure ENV ..."
echo "#################"
TIME_SERIES_INSIGHTS_SOURCE_RESOURCE_ID=$(az iot hub show -n $IOT_HUB_NAME -g $RESOURCE_GROUP --query id --output tsv)
SHARED_ACCES_KEY=$(az iot hub policy show -g $RESOURCE_GROUP --hub-name $IOT_HUB_NAME --n $TIME_SERIES_INSIGHTS_KEY_NAME --query primaryKey --output tsv)
STORAGE_ACCOUNT_KEY=$(az storage account keys list -n $STORAGE_ACCOUNT_NAME -g $RESOURCE_GROUP --query "[0].value" --output tsv)

echo "Create enviroment ..."
echo "#####################"
az timeseriesinsights environment longterm create \
    --data-retention 10 \
    --name $TIME_SERIES_INSIGHTS_ENVIRONMENT_NAME \
    --resource-group $RESOURCE_GROUP \
    --sku-name L1 \
    --sku-capacity 1 \
    --time-series-id-properties $TIME_SERIES_PARTITION_KEY\
    --storage-account-name $STORAGE_ACCOUNT_NAME \
    --storage-management-key $STORAGE_ACCOUNT_KEY
 
echo "Create event source ..."
echo "#######################"   
az timeseriesinsights event-source iothub create \
    --consumer-group-name $IOT_HUB_USER_GROUP \
    --environment-name $TIME_SERIES_INSIGHTS_ENVIRONMENT_NAME \
    --event-source-resource-id $TIME_SERIES_INSIGHTS_SOURCE_RESOURCE_ID \
    --key-name $TIME_SERIES_INSIGHTS_KEY_NAME \
    --name $TIME_SERIES_INSIGHTS_EVENT_SOURCE_NAME \
    --resource-group $RESOURCE_GROUP \
    --shared-access-key $SHARED_ACCES_KEY