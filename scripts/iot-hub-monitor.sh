source .env

echo '# Start monitoring ...'
echo '######################'
az iot hub monitor-events --hub-name $IOT_HUB_NAME